﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SubnauticaModdingHelper.CommandBindings
{
    public class FileCommands
    {
        static FileCommands()
        {
            SaveAll = new RoutedUICommand(
                "S_aveAll", "Save All", typeof(FileCommands),
                new InputGestureCollection{ new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+S")});
        }
        public static RoutedUICommand SaveAll { get; }
    }
}
