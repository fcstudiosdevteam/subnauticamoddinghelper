﻿using System.Linq;
using System.Windows.Input;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.Model
{
    //TODO link to OnEdited
    public class VersionDependency : BaseViewModel
    {
        private string _version;

        private string _modName;

        public string ModName
        {
            get => _modName;
            set
            {
                _modName = value;
                SetCurrentVersion(value);
            }
        }

        private void SetCurrentVersion(string value)
        {
            var version = QmodData.ModsList.SingleOrDefault(x => x.ModJson.Id == value)?.ModJson.Version;

            if (!string.IsNullOrEmpty(version))
            {
                Version = version;
            }
        }


        public string Version
        {
            get => _version;
            set
            {
                _version = value; 

            }
        }


        public ICommand DeleteDVCommand { get; set; }

        public VersionDependency()
        {
            DeleteDVCommand = new RelayParameterizedCommand(RemoveDButtonCommandMethod);
        }

        private void RemoveDButtonCommandMethod(object obj)
        {
            var modItem = (ModItem) obj;
            modItem.ModJson.DeleteVersionDependancy(this);
        }

        public override string ToString()
        {
            return $"ModName: {ModName} || Version: {Version}";
        }
    }
}
