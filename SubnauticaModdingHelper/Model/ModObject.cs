﻿using System.Windows.Input;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.Model
{
    public abstract class ModObject
    {
        public abstract string ModName { get; set; }
        public abstract ICommand DeleteDVCommand { get; set; }

        protected ModObject()
        {
            DeleteDVCommand = new RelayParameterizedCommand(RemoveDButtonCommandMethod);
        }

        public abstract void RemoveDButtonCommandMethod(object obj);
    }
}
