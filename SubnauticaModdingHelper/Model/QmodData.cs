﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using SubnauticaModdingHelper.Enumerators;
using SubnauticaModdingHelper.Model.UIElement;
using SubnauticaModdingHelper.View.Page;

namespace SubnauticaModdingHelper.Model
{
    public static class QmodData
    {
        private static WatchOperation _watcher;
        public static CustomTab SelectedTab { get; set; }
        
        public static string SBQmodPath => Properties.Settings.Default.SBQmodPath;

        public static string BZQmodPath => Properties.Settings.Default.BZQmodPath;
        public static string NexusAPIKey => Properties.Settings.Default.NexusAPIKey;

        public static string QModPath { get; set; }

        public static ObservableCollection<CustomTab> Tabs { get; set; } = new ObservableCollection<CustomTab>();

        public static ObservableCollection<ModItem> ModsList { get; set; } = new ObservableCollection<ModItem>();

        public static ObservableCollection<string> ModNameList { get; set; } = new ObservableCollection<string>();
        
        internal static void Initialize()
        {
            LoadMods();
        }
        
        internal static void LoadMods()
        {
            if (string.IsNullOrEmpty(QModPath)) return;

            if(_watcher == null)
            {
                _watcher = new WatchOperation();
                _watcher.OnNameChangedAction += OnRefreshTabs;
            }

            _watcher.Unsubscribe();

            _watcher.Watch();

            ModsList.Clear();

            string[] modDirs = Directory.GetDirectories(QModPath);

            foreach (string dir in modDirs)
            {
                var modjson = Directory.GetFiles(dir, "mod.json");
                if (modjson.Length > 0)
                {
                    var modItem = new ModItem
                    {
                        ModPath = dir, ModName = Path.GetFileName(dir), 
                        ModJsonPath = modjson[0]
                    };

                    if (modItem.ModJson != null)
                    {
                        ModsList.Add(modItem);
                    }
                }
            }
        }

        internal static void CloseNullTabs()
        {
            foreach (CustomTab customTab in Tabs)
            {
                var match = ModsList.SingleOrDefault(x => x.ModJson.Id == customTab.ModItem.ModJson.Id);

                if (match == null)
                {
                    CloseTab(customTab);
                    break;
                }
            }
        }

        internal static void CloseTab(CustomTab tabItem)
        {
            bool tryCloseResult = true;


            if (tabItem.IsEdited())
            {
                tryCloseResult = TryClose(tabItem.ModItem.SaveCommandMethod);
            }

            if (tryCloseResult)
            {
                CloseTabOperation(tabItem);
            }
        }

        public static void SetGameVersionQmodPath(GameVersions gameVersion)
        {
            GameVersion = gameVersion;
            bool tryCloseResult = true;
            
            if (IsAnyEdited())
            {
                tryCloseResult = TryClose(SaveAllTabs);
            }
            
            if (tryCloseResult)
            {
                Tabs.Clear();

                switch (gameVersion)
                {
                    case GameVersions.Subnautica:
                        QModPath = SBQmodPath;
                        break;
                    case GameVersions.BelowZero:
                        QModPath = BZQmodPath;
                        break;
                    case GameVersions.None:
                        ClearData();
                        break;
                }

                LoadMods();
            }
        }

        public static GameVersions GameVersion { get; set; }

        public static bool TryClose(Action method)
        {
            var result =
                MessageBox.Show(
                    "Changes where made! Would you like to save changes before closing?",
                    "Mod Not Saved", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);

            switch (result)
            {
                case MessageBoxResult.Cancel:
                    return false;
                case MessageBoxResult.Yes:
                    method?.Invoke();
                    break;
            }

            return true;
        }

        private static void ClearData()
        {
            Tabs.Clear();
            ModsList.Clear();
            ModNameList.Clear();
            QModPath = string.Empty;
        }

        private static void CloseTabOperation(CustomTab tabItem)
        {
            //Unsub from watcher
            tabItem?.ModItem?.UnSubscribe();

            //Remove tab
            Tabs.Remove(tabItem);
        }

        public static void OpenNewTab(ModItem value)
        {
            //See if tab is open.
            var match = Tabs.SingleOrDefault(x => x.ModItem == value);

            if (match != null)
            {
                match.IsSelected = true;
                return;
            }
            
            //Create a new mod page
            var modPage = new ModPage();

            var tab = new CustomTab
            {
                Header = Path.GetFileName(value.ModName),
                ModItem = value,
                IsSelected = true,
                Content = modPage
            };

            modPage.DirectoryFilesList = value.DirectoryItemsCollection;
            modPage.ModItem = value;
            value.Tab = tab;
            //Add new tab
            Tabs.Add(tab);
        }

        public static void OpenSettingsPage()
        {
            var headerName = "Preferences";

            //See if tab is open.
            var match = Tabs.SingleOrDefault(x => x.Header.Equals(headerName));

            if (match != null)
            {
                match.IsSelected = true;
                return;
            }

            //Create a new mod page
            var page = new SettingsPageView();

            var tab = new CustomTab
            {
                Header = headerName,
                IsSelected = true,
                Content = page
            };

            //Add new tab
            Tabs.Add(tab);
        }

        public static void RefreshTabs()
        {
            foreach (CustomTab customTab in Tabs)
            {
                customTab.Refresh();
            }
        }

        public static void AddNewModName(string modJsonId)
        {
            if (!ModNameList.Contains(modJsonId))
            {
                ModNameList.Add(modJsonId);
            }
        }

        public static void SaveSelectedTab()
        {
            foreach (CustomTab tab in Tabs)
            {
                if (tab.IsSelected)
                {
                    tab.ModItem.SaveCommandMethod();
                }
            }
        }

        public static void SaveAllTabs()
        {
            foreach (CustomTab tab in Tabs)
            {
                if (tab.IsEdited())
                {
                    tab.ModItem.SaveCommandMethod();
                }
            }
        }

        public static void OnRefreshTabs(RenamedEventArgs args)
        {
            foreach (CustomTab customTab in Tabs)
            {
                if (customTab.ModItem.ModPath == args.OldFullPath)
                {
                    customTab.RefreshData(args.FullPath);
                }

            }
        }

        public static bool IsAnyEdited()
        {
            var isEdited = Tabs.SingleOrDefault(x => x.IsEdited());
            return isEdited != null;
        }
    }
}