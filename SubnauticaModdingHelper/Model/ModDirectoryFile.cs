﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json;
using SubnauticaModdingHelper.Model.Helpers;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.Model
{
    public class ModDirectoryFile
    {
        public string FileName { get; set; }
        [JsonIgnore] public string FilePath { get; set; }

        private string _fullPath;

        [JsonIgnore]
        public string FullPath
        {
            get => _fullPath;
            set
            {
                _fullPath = value;
                CheckIfValidFile();
            }
        }

        public bool AllowedToPackage
        {
            get => _allowedToPackage;
            set
            {
                _allowedToPackage = value;
                OnUpdated();
            }
        }

        private void OnUpdated()
        {
            OnAllowedToPackageChanged?.Invoke();
        }

        [JsonIgnore] public string FileSize { get; set; }
        [JsonIgnore] public ICommand OpenLocationCommand { get; set; }
        [JsonIgnore] public ICommand DeleteFileCommand { get; set; }
        [JsonIgnore] public bool IsEnabled { get; set; }

        [JsonIgnore]
        public bool IsValidFile { get; set; } = true;

        [JsonIgnore]public Action OnAllowedToPackageChanged { get; set; }

        public int ModID { get; set; }

        private readonly string[] _allowedExtention = { ".json", ".png", ".dll", ".txt", ".fcs", "" };
        private bool _allowedToPackage;

        private void CheckIfValidFile()
        {
            if(FullPath == null) return;
            IsValidFile = _allowedExtention.Contains(Path.GetExtension(FullPath));
        }

        public ModDirectoryFile()
        {
            OpenLocationCommand = new RelayCommand(OpenLocationMethod);
            DeleteFileCommand = new RelayCommand(DeleteFileCommandMethod);
        }

        private void DeleteFileCommandMethod()
        {
            DirectoryHelpers.SafeFileDelete(FullPath);
        }

        private void OpenLocationMethod()
        {
            try
            {
                var args = $"/e, /select, \"{FullPath}\"";
                var info = new ProcessStartInfo {FileName = "explorer", Arguments = args};
                Process.Start(info);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        
        public override string ToString()
        {
            return $"File Name : {FileName}";
        }
    }
}
