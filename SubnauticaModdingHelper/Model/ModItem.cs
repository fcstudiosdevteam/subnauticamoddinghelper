﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Ionic.Zip;
using Microsoft.WindowsAPICodePack.Dialogs;
using Pathoschild.FluentNexus;
using Pathoschild.FluentNexus.Models;
using SubnauticaModdingHelper.Enumerators;
using SubnauticaModdingHelper.Model.Helpers;
using SubnauticaModdingHelper.Model.UIElement;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.Model
{
    public class ModItem : BaseViewModel
    {
        private FileSystemWatcher _watcher;
        private string _modJsonPath;
        private string _fileStructureSave;
        public bool HasIssues { get; set; }
        public string ModName { get; set; }
        public string ModPath { get; set; }
        public string ModJsonPath
        {
            get => _modJsonPath;
            set
            {
                _modJsonPath = value;
                RefreshModConfig();
            }
        }
        public ICommand SaveModJsonCommand { get; set; }
        public ICommand ShowDetailCommand { get; set; }
        public ICommand PackageModCommand { get; set; }
        public ICommand FixIssuesCommand { get; set; }
        public ModConfig ModJson { get; set; }
        public ObservableCollection<ModDirectoryFile> DirectoryItemsCollection { get; set; } = new ObservableCollection<ModDirectoryFile>();
        public ObservableCollection<IssueReport> IssueReports { get; set; } = new ObservableCollection<IssueReport>();
        public CustomTab Tab { get; set; }

        public Mod NexusJson { get; set; }

        public int NexusModID
        {
            get => _nexusModId;
            set
            {
                _nexusModId = value;

                if (_saveLoaded)
                {
                    OnEditedChanged();
                }
            }
        }

        public ICommand NexusCommandClick{ get; set; }


        private async Task GetNexusData()
        {
            try
            {
                NexusJson = await App.NexusClient.Mods.GetMod(QmodData.GameVersion.ToString(), NexusModID);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public ModItem()
        {
            ShowDetailCommand = new RelayCommand(OpenNewTab);
            SaveModJsonCommand = new RelayCommand(SaveCommandMethod);
            PackageModCommand = new RelayCommand(PackageModCommandMethod);
            FixIssuesCommand = new RelayCommand(FixIssuesCommandMethod);
            NexusCommandClick = new AsyncCommand(NexusCommandClickMethod);
        }

        private async Task NexusCommandClickMethod()
        {
           await GetNexusData();
        }

        private void FixIssuesCommandMethod()
        {
            for (int i = 0; i < IssueReports.Count; i++)
            {
                switch (IssueReports[0].IssueReportType)
                {
                    case IssueReportType.VersionMissMatch:
                        ModJson.Version = IssueReports[0].DLLVersion;
                        break;
                    case IssueReportType.MissingDependancy:
                        break;
                    case IssueReportType.ModDisabled:
                        ModJson.Enable = true;
                        break;
                    case IssueReportType.Unknown:
                        break;
                    case IssueReportType.MissingDllExtension:

                        if (ModJson.AssemblyName.EndsWith("."))
                        {
                            var name = ModJson.AssemblyName.Substring(0, ModJson.AssemblyName.Length - 1);
                            ModJson.AssemblyName = $"{name}.dll";
                        }
                        else
                        {
                            ModJson.AssemblyName = $"{ModJson.AssemblyName}.dll";
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                UpdateHasIssues();
            }
        }

        private void CheckForIssues()
        {
            IssueReports.Clear();

            if (string.IsNullOrEmpty(ModJson.AssemblyName)) return;

            var dllPath = Path.Combine(ModPath, ModJson.AssemblyName);

            if (File.Exists(dllPath))
            {
                if (!ApplicationHelpers.VersionCompare(ModJson.Version, dllPath, out string message))
                {
                    IssueReports.Add(new IssueReport { IssueReportType = IssueReportType.VersionMissMatch, DLLVersion = ApplicationHelpers.GetVersionNumber(dllPath) });
                }
            }


            if (!ModJson.Enable)
            {
                IssueReports.Add(new IssueReport { IssueReportType = IssueReportType.ModDisabled });
            }

            if (!Path.GetExtension(ModJson.AssemblyName).Equals(".dll"))
            {
                IssueReports.Add(new IssueReport{IssueReportType = IssueReportType.MissingDllExtension});
            }

            UpdateHasIssues();
            //TODO Use HomeViewModel to look for the dependencies
        }
        private void UpdateHasIssues()
        {
            HasIssues = IssueReports.Count > 0;
        }
        
        private void PackageModCommandMethod()
        {
            if (HasIssues)
            {
                var result = MessageBox.Show(
                    "There are issues that need to be fixed before you are able to package this mod. Would you like to bypass these issues and continue?",
                    "Issues were found", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }


            using (CommonSaveFileDialog sfd = new CommonSaveFileDialog())
            {
                sfd.DefaultFileName = $"{ModName}_{ModJson.Version}";
                sfd.DefaultExtension = "zip";
                sfd.AlwaysAppendDefaultExtension = true;
                sfd.Filters.Add(new CommonFileDialogFilter("Zip File", "*.zip"));
                
                string sourceDirectory = ModPath;
                string targetDirectory = Path.Combine(Path.GetTempPath(), ModName);

                if (sfd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    try
                    {
                        DirectoryHelpers.Copy(sourceDirectory, targetDirectory);

                        for (int i = DirectoryItemsCollection.Count - 1; i >= 0; i--)
                        {
                            var file = DirectoryItemsCollection[i];
                            if (!file.AllowedToPackage)
                            {
                                var path = GetModPath(file.FullPath);

                                if (!string.IsNullOrEmpty(path))
                                {
                                    DirectoryHelpers.SafeFileDelete(Path.Combine(targetDirectory, path), false);
                                }
                            }
                        }

                        using (ZipFile zip = new ZipFile())
                        {
                            zip.AddDirectory(targetDirectory, ModName);

                            zip.Save(Path.Combine(ModPath, sfd.FileName));
                        }
                    }
                    finally
                    {
                        DirectoryHelpers.SafeDeleteDirectory(targetDirectory);
                    }
                }
            }
        }
        
        private string GetModPath(string path)
        {
            var array = path.Split(Path.DirectorySeparatorChar).ToList();
            Queue qt = new Queue();

            foreach (string name in array)
            {
                qt.Enqueue(name);
            }
            
            for (int i = qt.Count - 1; i >= 0; i--)
            {
                if (qt.Peek().ToString() != ModName)
                {
                    qt.Dequeue();
                }
                else
                {
                    qt.Dequeue();
                    array.Clear();
                    foreach (string name in qt)
                    {
                        array.Add(name);
                        return Path.Combine(array.ToArray());
                    }
                    break;
                }
            }
            return string.Empty;
        }

        public void SaveCommandMethod()
        {
            ModJson.SaveModConfig();
            SaveFCSConfigFile();
            Tab.UpdateLabel(false);
        }

        private void OpenNewTab()
        {
            QmodData.OpenNewTab(this);
            UpdateFiles();
            Watch();
            RefreshModConfig();
            _fileStructureSave = Path.Combine(ModPath, "FCSModdingUtilitySave.fcs");
            CheckForIssues();
        }

        private void OnEditedChanged()
        {
            Tab.UpdateLabel(true);
            CheckForIssues();
        }

        private void RefreshModConfig()
        {
            if (ModJson != null)
            {
                ModJson.OnEditedChanged -= OnEditedChanged;
            }

            ModJson = DirectoryHelpers.LoadModConfig(ModJsonPath, this);
            
            if(ModJson != null)
            {
                ModJson.ModJsonPath = ModJsonPath;
                ModJson.Initialize();
                ModJson.OnEditedChanged += OnEditedChanged;
                QmodData.AddNewModName(ModJson.Id);
            }
        }

        private bool mSubscribed;
        private int _nexusModId;
        private bool _saveLoaded;

        private void Watch()
        {
            _watcher = new FileSystemWatcher();
            _watcher.Path = ModPath;
            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.Size
                                    | NotifyFilters.DirectoryName;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _watcher.Created += OnChanged;
            _watcher.Deleted += OnChanged;
            _watcher.Renamed += OnChanged;
            _watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            App.Current.Dispatcher?.Invoke(UpdateFiles);
            App.Current.Dispatcher?.Invoke(RefreshModConfig);
            App.Current.Dispatcher?.Invoke(CheckForIssues);
        }

        public void UpdateFiles()
        {

            foreach (ModDirectoryFile modDirectoryFile in DirectoryItemsCollection)
            {
                modDirectoryFile.OnAllowedToPackageChanged -= OnEditedChanged;
            }

            DirectoryItemsCollection.Clear();
            
            var files = Directory.GetFiles(ModPath, "*", SearchOption.AllDirectories);
            
            var saveFiles = Directory.GetFiles(ModPath, "*.fcs", SearchOption.AllDirectories);

            FCSModConfiguration save = null;

            if (saveFiles.Length > 0)
            {
                save = DirectoryHelpers.LoadConfigurationData(saveFiles[0]);
                NexusModID = save.ModId;
            }
            
            foreach (string path in files)
            {
                if(string.IsNullOrEmpty(path)) continue;

                var isSaveFile = Path.GetExtension(path).Equals(".fcs");
              
                bool allowedToPackage = !isSaveFile;

                var saveData = save?.DirectoryFiles?.SingleOrDefault(x => x.FileName.Equals(Path.GetFileName(path)));

                if (saveData != null && !isSaveFile)
                {
                    allowedToPackage = saveData.AllowedToPackage;
                }
                else if (saveData == null && !isSaveFile)
                {
                    allowedToPackage = true;
                }

                var modFile = new ModDirectoryFile
                {
                    AllowedToPackage = allowedToPackage,
                    FullPath = path,
                    FileName = Path.GetFileName(path),
                    FilePath = Directory.GetParent(path).FullName,
                    FileSize = DirectoryHelpers.IsDirectory(path)
                        ? DirectoryHelpers.GetDirectorySize(path)
                        : DirectoryHelpers.GetFileSize(path),
                    IsEnabled = !isSaveFile
                };

                modFile.OnAllowedToPackageChanged += OnEditedChanged;

                DirectoryItemsCollection.Add(modFile);

                _saveLoaded = true;
            }
        }
        
        public void UnSubscribe()
        {

            ModJson.OnEditedChanged -= OnEditedChanged;

            if (_watcher == null) return;

            _watcher.Changed -= OnChanged;
            _watcher.Created -= OnChanged;
            _watcher.Deleted -= OnChanged;
            _watcher.Renamed -= OnChanged;
        }

        public void SaveFCSConfigFile()
        {
            var saveData = new FCSModConfiguration(NexusModID,DirectoryItemsCollection.ToList());
            DirectoryHelpers.SaveConfigurationData(_fileStructureSave, saveData);
        }

        public void UpdateData(string argsFullPath)
        {
            ModName = Path.GetFileName(argsFullPath);
            ModPath = argsFullPath;
            UpdateFiles();
        }

        public override string ToString()
        {
            return $"ModName: {ModName} || {ModJson.Version}";
        }
    }
}
