﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.Model
{
    public class ModConfig : INotifyPropertyChanged
    {
        private Dictionary<string, string> _versionDependencies;
        private ObservableCollection<LoadAfter> _loadAfterCollection;
        private ObservableCollection<LoadBefore> _loadBeforeCollection;
        private ObservableCollection<Dependencies> _dependencyListCollection;
        private List<string> _loadBefore;
        private List<string> _loadAfter;
        private List<string> _dependancies;
        private bool _initialized;
        private string _id;
        private string _displayName;
        private string _author;
        private string _version;
        private bool _enable;
        private string _game;
        private string _assemblyName;
        private string _entryMethod;
        private ObservableCollection<VersionDependency> _versionDependenciesList = new ObservableCollection<VersionDependency>();

        public string Id
        {
            get => _id;
            set
            {
                _id = value;
                UpdateHasChanged();
            }
        }

        public string DisplayName
        {
            get => _displayName;
            set
            {
                _displayName = value;
                UpdateHasChanged();
            }
        }

        public string Author
        {
            get => _author;
            set
            {
                _author = value;
                UpdateHasChanged();
            }
        }

        public string Version
        {
            get => _version;
            set
            {
                _version = value;
                UpdateHasChanged();
                OnPropertyChanged(nameof(Version));
            }
        }

        public bool Enable
        {
            get => _enable;
            set
            {
                _enable = value;
                UpdateHasChanged();
                OnPropertyChanged(nameof(Enable));

            }
        }

        public string Game
        {
            get => _game;
            set
            {
                _game = value;
                UpdateHasChanged();
            }
        }

        public string AssemblyName
        {
            get => _assemblyName;
            set
            {
                _assemblyName = value;

                UpdateHasChanged();
                OnPropertyChanged(nameof(AssemblyName));
            }
        }

        public string EntryMethod
        {
            get => _entryMethod;
            set
            {
                _entryMethod = value;
                UpdateHasChanged();
            }
        }

        public Dictionary<string, string> VersionDependencies
        {
            get => _versionDependencies;
            set
            {
                _versionDependencies = value;
                CreateVersionDependencies();
            }
        } 
        public List<string> LoadBefore
        {
            get => _loadBefore;
            set
            {
                _loadBefore = value;
                UpdateLoadBeforeData();
            }
        }
        public List<string> LoadAfter
        {
            get => _loadAfter;
            set
            {
                _loadAfter = value;
                UpdateLoadAfterData();
            }
        }
        public List<string> Dependencies
        {
            get => _dependancies;
            set
            {
                _dependancies = value;
                GenerateDependencies();
            }
        }

        [JsonIgnore] public Action OnEditedChanged { get; set; }

        [JsonIgnore]
        public ObservableCollection<Dependencies> DependenciesList
        {
            get
            {
                if (_dependencyListCollection == null)
                {
                    _dependencyListCollection = new ObservableCollection<Dependencies>();
                }
                return _dependencyListCollection;
            }
            set
            {
                _dependencyListCollection = value;
                UpdateHasChanged();
            }
        }

        [JsonIgnore]
        public ObservableCollection<LoadBefore> LoadBeforeCollection
        {
            get
            {
                if (_loadBeforeCollection == null)
                {
                    _loadBeforeCollection = new ObservableCollection<LoadBefore>();
                }
                return _loadBeforeCollection;

            }
            set
            {
                _loadBeforeCollection = value;
                UpdateHasChanged();
            }
        }

        [JsonIgnore]
        public ObservableCollection<LoadAfter> LoadAfterCollection
        {
            get
            {
                if (_loadAfterCollection == null)
                {
                    _loadAfterCollection = new ObservableCollection<LoadAfter>();
                }
                return _loadAfterCollection;

            }
            set
            {
                _loadAfterCollection = value;
                UpdateHasChanged();
            }
        }

        [JsonIgnore]
        public ObservableCollection<VersionDependency> VersionDependenciesList
        {
            get => _versionDependenciesList;
            set
            {
                _versionDependenciesList = value;
                UpdateHasChanged();
            }
        }

        [JsonIgnore] public string ModJsonPath { get; set; }

        private void CreateVersionDependencies()
        {
            if (VersionDependencies == null) return;
            foreach (KeyValuePair<string, string> dependency in VersionDependencies)
            {
                VersionDependenciesList.Add(new VersionDependency { ModName = dependency.Key, Version = dependency.Value });
            }
        }

        public void DeleteVersionDependancy(VersionDependency versionDependency)
        {

            if (VersionDependencies != null)
            {
                foreach (KeyValuePair<string, string> dependency in VersionDependencies)
                {
                    if (dependency.Key == versionDependency.ModName && dependency.Value == versionDependency.Version)
                    {
                        VersionDependencies.Remove(dependency.Key);
                        break;
                    }
                }
            }

            VersionDependenciesList.Remove(versionDependency);
        }

        public void DeleteDependency(Dependencies modObject)
        {
            if (Dependencies != null)
            {
                foreach (string dependency in Dependencies)
                {
                    if (dependency == modObject.ModName)
                    {
                        Dependencies.Remove(dependency);
                        break;
                    }
                }
            }
            
            DependenciesList.Remove(modObject);
        }

        public void DeleteLoadBefore(LoadBefore modObject)
        {
            if (LoadBefore != null)
            {
                foreach (string dependency in LoadBefore)
                {
                    if (dependency == modObject.ModName)
                    {
                        LoadBefore.Remove(dependency);
                        break;
                    }
                }
            }

            LoadBeforeCollection.Remove(modObject);
        }

        public void DeleteLoadAfter(LoadAfter modObject)
        {
            if (LoadAfter != null)
            {
                foreach (string dependency in LoadAfter)
                {
                    if (dependency == modObject.ModName)
                    {
                        LoadAfter.Remove(dependency);
                        break;
                    }
                }
            }

            LoadAfterCollection.Remove(modObject);
        }

        private void GenerateDependencies()
        {
            if (Dependencies == null) return;
            foreach (string dependency in Dependencies)
            {
                DependenciesList.Add(new Dependencies { ModName = dependency });
            }
        }

        internal void UpdateLoadBeforeData()
        {
            if (LoadBefore != null)
            {
                foreach (string loadBefore in LoadBefore)
                {
                    LoadBeforeCollection.Add(new LoadBefore { ModName = loadBefore });
                }
            }
        }

        internal void UpdateLoadAfterData()
        {
            if (LoadAfter != null)
            {
                foreach (string loadAfter in LoadAfter)
                {
                    LoadAfterCollection.Add(new LoadAfter { ModName = loadAfter });
                }
            }
        }

        internal void SaveModConfig()
        {
            try
            {
                var originalJson = File.ReadAllText(ModJsonPath);

                // If this is null there isn't anything to process
                if (!(JsonConvert.DeserializeObject(originalJson, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }) is JObject jObject))
                {
                    //ViewModelApplication.DefaultError("Save Failed Original Json returned null");
                    return;
                }

                SetProperty(jObject, nameof(Id), Id);

                SetProperty(jObject, nameof(Enable), Enable);

                SetProperty(jObject, nameof(DisplayName), DisplayName);

                SetProperty(jObject, nameof(Author), Author);

                SetProperty(jObject, nameof(Version), Version);

                SetProperty(jObject, nameof(Game), Game);

                SetProperty(jObject, nameof(AssemblyName), AssemblyName);

                SetProperty(jObject, nameof(EntryMethod), EntryMethod);

                UpdateDependencies(jObject);

                SetProperty(jObject, nameof(VersionDependencies), VersionDependencies);
                
                SetProperty(jObject, nameof(Dependencies), Dependencies);
                
                SetProperty(jObject, nameof(LoadAfter), LoadAfter);
                
                SetProperty(jObject, nameof(LoadBefore), LoadBefore);
                
                var updatedJsonString = jObject.ToString();

                File.WriteAllText(ModJsonPath, updatedJsonString);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //ViewModelApplication.DefaultError(e.Message);
            }
        }


        private void SetProperty(JObject jObject,string propertyName ,object property)
        {
            var result = jObject.TryGetValue(propertyName, out var value);

            if (property == null)
            {
                jObject.Remove(propertyName);
                return;
            }

            if (value != null && value.Type == JTokenType.String)
            {
                if (string.IsNullOrEmpty(value.ToString()))
                {
                    jObject.Remove(propertyName);
                    return;
                }
            }
            
            if (!result)
            {
                AddMissingProperty(jObject, propertyName);
            }

            var token = value ?? jObject.SelectToken(propertyName);
            token?.Replace(JToken.FromObject(property));

        }
        
        private void UpdateDependencies(JObject jObject)
        {
            if (DependenciesList.Count <= 0)
            {
                Dependencies = null;
            }

            if (VersionDependenciesList.Count <= 0)
            {
                VersionDependencies = null;
            }

            if (LoadAfterCollection.Count <= 0)
            {
                LoadAfter = null;
            }

            if (LoadBeforeCollection.Count <= 0)
            {
                LoadBefore = null;
            }

            Dependencies?.Clear();

            foreach (var dependency in DependenciesList)
            {
                if(Dependencies == null)
                {
                    Dependencies = new List<string>();
                }

                Dependencies?.Add(dependency.ModName);
            }

            VersionDependencies?.Clear();
            foreach (VersionDependency versionDependency in VersionDependenciesList)
            {
                if(VersionDependencies == null)
                {
                    VersionDependencies = new Dictionary<string, string>();
                }

                VersionDependencies.Add(versionDependency.ModName, versionDependency.Version);
            }

            LoadAfter?.Clear();
            foreach (LoadAfter modObject in LoadAfterCollection)
            {
                if(LoadAfter == null)
                {
                    LoadAfter = new List<string>();
                }
                LoadAfter?.Add(modObject.ModName);
            }

            LoadBefore?.Clear();
            foreach (LoadBefore modObject in LoadBeforeCollection)
            {
                if(LoadBefore == null)
                {
                    LoadBefore = new List<string>();
                }

                LoadBefore?.Add(modObject.ModName);
            }
        }

        private void AddMissingProperty(JObject jObject ,string propertyName, JToken value = null)
        {
            jObject.Add(propertyName, value);
        }

        public void Initialize()
        {
            _initialized = true;
        }

        private void UpdateHasChanged()
        {
            if (_initialized)
            {
                OnEditedChanged?.Invoke();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
