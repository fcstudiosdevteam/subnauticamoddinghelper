﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SubnauticaModdingHelper.Model
{
    public class LoadAfter : ModObject
    {
        public override string ModName { get; set; }
        public override ICommand DeleteDVCommand { get; set; }

        public override void RemoveDButtonCommandMethod(object obj)
        {
            var modItem = (ModItem)obj;
            modItem.ModJson.DeleteLoadAfter(this);
        }
    }
}
