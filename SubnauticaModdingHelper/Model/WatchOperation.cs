﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SubnauticaModdingHelper.Model
{
    public class WatchOperation : INotifyPropertyChanged
    {
        private FileSystemWatcher _watcher;
        public event PropertyChangedEventHandler PropertyChanged;

        public void Watch()
        {
            if (QmodData.QModPath == null) return;

            _watcher = new FileSystemWatcher();
            _watcher.Path = QmodData.QModPath;
            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.Size
                                    | NotifyFilters.DirectoryName;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _watcher.Created += OnChanged;
            _watcher.Deleted += OnChanged;
            _watcher.Renamed += OnNameChanged;
            _watcher.EnableRaisingEvents = true;
        }

        private void OnNameChanged(object sender, RenamedEventArgs e)
        {
            OnNameChangedAction?.Invoke(e);
        }

        public Action<RenamedEventArgs> OnNameChangedAction { get; set; }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            App.Current.Dispatcher.Invoke(QmodData.LoadMods);
            App.Current.Dispatcher.Invoke(QmodData.CloseNullTabs);
            //TODO Check idf tab is modItem is null
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Unsubscribe()
        {
            if (_watcher != null)
            {
                _watcher.Changed -= OnChanged;
                _watcher.Created -= OnChanged;
                _watcher.Deleted -= OnChanged;
                _watcher.Renamed -= OnNameChanged;
            }
        }
    }
}
