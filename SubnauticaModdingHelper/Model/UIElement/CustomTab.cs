﻿using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace SubnauticaModdingHelper.Model.UIElement
{
    public class CustomTab : TabItem
    {
        /// <summary>
        /// Boolean to state if the tab has content has been edited
        /// </summary>
        private bool _contentEdited;

        public ModItem ModItem { get; set; }

        public void Refresh()
        {
            ModItem.UpdateFiles();
        }

        public void UpdateLabel(bool value)
        {
            var header = Header.ToString();

            var result = header.Substring(header.Length - 1);

            if(!value)
            {
                Header = header.Remove(header.Length - 1, 1);
                _contentEdited = false;
                return;
            }

            if (!result.Equals("*") && value)
            {
                Header = $"{Header}*";
                _contentEdited = true;
            }
        }

        public bool IsEdited()
        {
            return _contentEdited;
        }

        public void RefreshData(string argsFullPath)
        {

            Application.Current.Dispatcher.Invoke(new System.Action(() =>
            {
                var header = Header.ToString();

                var result = header.Substring(header.Length - 1);

                if (result.Equals("*"))
                {
                    Header = $"{Path.GetFileName(argsFullPath)}*";
                    _contentEdited = true;
                }
                else
                {
                    Header = Path.GetFileName(argsFullPath);
                }

                ModItem.UpdateData(argsFullPath);
            }));

            
        }
    }
}
