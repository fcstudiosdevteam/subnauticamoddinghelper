﻿using System.Windows.Input;

namespace SubnauticaModdingHelper.Model
{
    public class LoadBefore : ModObject
    {
        public override string ModName { get; set; }
        public override ICommand DeleteDVCommand { get; set; }

        public override void RemoveDButtonCommandMethod(object obj)
        {
            var modItem = (ModItem)obj;
            modItem.ModJson.DeleteLoadBefore(this);
        }
    }
}
