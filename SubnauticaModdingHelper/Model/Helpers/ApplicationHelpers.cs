﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SubnauticaModdingHelper.Model.Helpers
{
    public class ApplicationHelpers
    {
        public static bool VersionCompare(string version, string dllPath, out string message)
        {
            try
            {
                string versionDll = GetVersionNumber(dllPath);

                if (version.Length < 5 || versionDll.Length < 5)
                {
                    message = "Incorrect Version expecting (Semantic Version).";
                    return false;
                }

                var version1 = new Version(version);
                var version2 = new Version(versionDll);

                version = VersionToSematic(version1);
                versionDll = VersionToSematic(version2);

                version1 = new Version(version);
                version2 = new Version(versionDll);

                var result = version1.CompareTo(version2);

                if (result > 0)
                {
                    message = "version 1 is greater";
                }
                else if (result < 0)
                {
                    message = "version 2 is greater";
                }
                else
                {
                    message = "versions are equal";
                    return true;
                }
            }
            catch (Exception e)
            {
                message = string.Empty;
                //ViewModelApplication.DefaultError(e.Message);
                return false;
            }

            return false;
        }

        public static string VersionToSematic(Version version)
        {
            var major = version.Major == -1 ? 0 : version.Major;
            var minor = version.Minor == -1 ? 0 : version.Minor;
            var build = version.Build == -1 ? 0 : version.Build;

            return $"{major}.{minor}.{build}";
        }

        public static string VersionToSematic(string versionInfoProductVersion)
        {
            try
            {
                var version = new Version(versionInfoProductVersion);
                return $"{version.Major}.{version.Minor}.{version.Build}";
            }
            catch (Exception e)
            {
                //ViewModelApplication.DefaultError(e.Message);
                return string.Empty;
            }
        }

        public static string GetVersionNumber(string dllLocation)
        {
            if (!File.Exists(dllLocation)) return String.Empty;
            if (string.IsNullOrEmpty(dllLocation)) return "N/A";
            var versionInfo = FileVersionInfo.GetVersionInfo(dllLocation);
            return ApplicationHelpers.VersionToSematic(versionInfo.ProductVersion); // Will typically return "1.0.0" in your case
        }

    }
}
