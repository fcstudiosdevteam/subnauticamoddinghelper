﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubnauticaModdingHelper.Model
{
    public class FCSModConfiguration
    {
        public FCSModConfiguration()
        {
            
        }

        public FCSModConfiguration(int modId, List<ModDirectoryFile> files)
        {
            ModId = modId;
            DirectoryFiles = files;
        }
        public int ModId { get; set; }
        public List<ModDirectoryFile> DirectoryFiles { get; set; }                      
    }
}
