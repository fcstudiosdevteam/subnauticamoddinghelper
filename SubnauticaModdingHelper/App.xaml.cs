﻿using System.Windows;
using Pathoschild.FluentNexus;
using SubnauticaModdingHelper.Model;

namespace SubnauticaModdingHelper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static NexusClient NexusClient { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            QmodData.Initialize();
            NexusClient = new NexusClient(SubnauticaModdingHelper.Properties.Settings.Default.NexusAPIKey, "SubnuaticaModdingHelper", "1.0.0");
        }
    }
}
