﻿using System;
using System.Globalization;
using System.Windows.Data;
using SubnauticaModdingHelper.Enumerators;

namespace SubnauticaModdingHelper.Converters
{
    public class GameToGameVersionsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value?.ToString().ToLower())
            {
                case "subnautica":
                    return 1;
                case "below zero":
                    return 2;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case 0:
                    return GameVersions.None;
                case 1:
                    return GameVersions.Subnautica;
                case 2:
                    return GameVersions.BelowZero;
            }

            return GameVersions.None;
        }
    }
}

