﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SubnauticaModdingHelper.Converters
{
    public class GameToIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value?.ToString().ToLower())
            {
                case "none":
                    return 0;
                case "both":
                    return 1;
                case "subnautica":
                    return 2;
                case "belowzero":
                    return 3;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case 0:
                    return string.Empty;
                case 1:
                    return "Both";
                case 2:
                    return "Subnautica";
                case 3:
                    return "BelowZero";
            }

            return string.Empty;
        }
    }
}

