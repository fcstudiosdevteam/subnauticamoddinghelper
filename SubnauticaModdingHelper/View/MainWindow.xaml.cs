﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection.Emit;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using SubnauticaModdingHelper.Model;
using SubnauticaModdingHelper.View.Page;

namespace SubnauticaModdingHelper.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ModCollectionListBox.ItemsSource).Refresh();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ModCollectionListBox.ItemsSource).Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(SearchTxtB.Text))
                return true;

            var resItem = (ModItem)item;

            CultureInfo culture  = CultureInfo.CurrentCulture;
            
            return culture.CompareInfo.IndexOf(resItem.ModName, SearchTxtB.Text, CompareOptions.IgnoreCase) >= 0;

        }

        private void Close_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; //< --Set this to true to enable bindings.
        }

        private void New_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(QmodData.QModPath))
            {
                MessageBox.Show("Please select a game to create the mod folder", "No game selected",
                    MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            var newMod = new NewModPageView();
            newMod.Show();
        }

        private void Save_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QmodData.SaveSelectedTab();
        }

        private void SaveAll_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QmodData.SaveAllTabs();
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            var isEdited = QmodData.IsAnyEdited();

            if (isEdited)
            {

                var message =
                    MessageBox.Show(
                        "Changes were made and are not saved are you sure you would like to close without saving?",
                        "Changes where made!", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                switch (message)
                {
                    case MessageBoxResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }
    }
}
