﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using SubnauticaModdingHelper.Model;
using SubnauticaModdingHelper.Model.UIElement;

namespace SubnauticaModdingHelper.View.Page
{
    /// <summary>
    /// Interaction logic for ModPage.xaml
    /// </summary>
    public partial class ModPage : UserControl
    {
        public ModPage()
        {
            InitializeComponent();
        }

        public ObservableCollection<ModDirectoryFile> DirectoryFilesList
        {
            get => (ObservableCollection<ModDirectoryFile>)GetValue(DirectoryFilesListProperty);
            set => SetValue(DirectoryFilesListProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsExpanded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DirectoryFilesListProperty =
            DependencyProperty.Register(nameof(DirectoryFilesList), typeof(ObservableCollection<ModDirectoryFile>), typeof(ModPage), new PropertyMetadata());

        public ModItem ModItem
        {
            get => (ModItem)GetValue(ModItemProperty);
            set => SetValue(ModItemProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsExpanded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModItemProperty =
            DependencyProperty.Register(nameof(ModItem), typeof(ModItem), typeof(ModPage), new PropertyMetadata());
    }
}
