﻿namespace SubnauticaModdingHelper.Enumerators
{
    public enum IssueReportType
    {
        VersionMissMatch,
        MissingDependancy,
        ModDisabled,
        Unknown,
        MissingDllExtension
    }
}
