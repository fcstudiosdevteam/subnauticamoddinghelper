﻿using System.Windows;
using System.Windows.Input;
using SubnauticaModdingHelper.Model.Helpers;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.ViewModel.Pages
{
    public class NewModPageVieWModel : BaseViewModel
    {
        public string Name { get; set; }
        public string ID { get; set; }
        public bool CreateAssetFolder { get; set; }
        public ICommand CreateModClick {get; set; }


        public NewModPageVieWModel()
        {
            CreateModClick = new RelayParameterizedCommand(CreateModMethod);
        }

        private void CreateModMethod(object obj)
        {

            DirectoryHelpers.CreateModFolder(Name,ID, CreateAssetFolder);

            if (obj is Window window)
            {
                window.Close();
            }
        }
    }
}
