﻿using System.Windows.Input;
using SubnauticaModdingHelper.Enumerators;
using SubnauticaModdingHelper.Model;
using SubnauticaModdingHelper.Model.UIElement;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.ViewModel.Pages
{
    public class MainWindowViewModel : BaseViewModel
    {

        private static GameVersions _gameVersion;

        public static GameVersions GameVersion
        {
            get => _gameVersion;
            set
            {
                _gameVersion = value;
                QmodData.SetGameVersionQmodPath(value);
            }
        }

        public MainWindowViewModel()
        {
            OpenSettingsCommand = new RelayCommand(OpenSettingsMethod);
        }

        private void OpenSettingsMethod()
        {
            QmodData.OpenSettingsPage();
        }

        public ICommand OpenSettingsCommand { get; set; }
        private CustomTab _selectedTab;

        public CustomTab SelectedTab
        {
            get => _selectedTab;
            set
            {
                _selectedTab = value;
                QmodData.SelectedTab = value;
            }
        }
    }
}
