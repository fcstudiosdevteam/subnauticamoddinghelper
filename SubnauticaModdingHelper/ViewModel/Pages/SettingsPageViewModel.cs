﻿using System.IO;
using System.Windows;
using System.Windows.Input;
using Microsoft.WindowsAPICodePack.Dialogs;
using SubnauticaModdingHelper.Enumerators;
using SubnauticaModdingHelper.Model;
using SubnauticaModdingHelper.ViewModel.Base;

namespace SubnauticaModdingHelper.ViewModel.Pages
{
    internal class SettingsPageViewModel : BaseViewModel
    {
        private string _nexusApiKey = QmodData.NexusAPIKey;
        private const string QmodsFolderName = "qmods";

        public SettingsPageViewModel()
        {
            BrowseBZClick = new RelayCommand(BrowseBZClickMethod);
            BrowseSBClick = new RelayCommand(BrowseSBClickMethod);
        }

        public string BZPath { get; set; } = QmodData.BZQmodPath;

        public string  SBPath { get; set; } = QmodData.SBQmodPath;

        public string NexusApiKey
        {
            get => _nexusApiKey;
            set
            {
                _nexusApiKey = value;
                Properties.Settings.Default.NexusAPIKey = value;
                SaveSettings();
            }
        }

        private void BrowseSBClickMethod()
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.IsFolderPicker = true;
                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    if (Path.GetFileName(ofd.FileName).ToLower().Equals(QmodsFolderName))
                    {
                        Properties.Settings.Default.SBQmodPath = ofd.FileName;
                        SBPath = ofd.FileName;
                        SaveSettings();
                        QmodData.SetGameVersionQmodPath(GameVersions.Subnautica);
                    }
                    else
                    {
                        var messageResult =
                            MessageBox.Show(
                                "Incorrect directory choosen. Please select the QMods folder in the Game Directory",
                                "Wrong Directory", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
                        if (messageResult == MessageBoxResult.OK)
                        {
                            BrowseSBClickMethod();
                        }
                    }

                }
            }
        }

        private void BrowseBZClickMethod()
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.IsFolderPicker = true;

                
                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {

                    if (Path.GetFileName(ofd.FileName).ToLower().Equals(QmodsFolderName))
                    {
                        Properties.Settings.Default.BZQmodPath = ofd.FileName;
                        BZPath = ofd.FileName;
                        SaveSettings();
                        QmodData.SetGameVersionQmodPath(GameVersions.BelowZero);
                    }
                    else
                    {
                        var messageResult =
                            MessageBox.Show(
                                "Incorrect directory choosen. Please select the QMods folder in the Game Directory",
                                "Wrong DIrectory", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
                        if (messageResult == MessageBoxResult.OK)
                        {
                            BrowseSBClickMethod();
                        }
                    }
                }
            }
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.Save();
        }

        public ICommand BrowseBZClick { get; set; }
        public ICommand BrowseSBClick { get; set; }
    }
}
